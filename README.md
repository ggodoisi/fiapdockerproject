# Arquitetura de Storage, DataCenter, Virtualização e Microcontainers

## Trabalho da Turma 35ASO
* Rafael RM332765
* Felipe RM332957
* Rodrigo RM332290
* Gabriel RM332235

O objetivo deste trabalho é criar de forma simples uma estrutura de docker-compose utilizando um container rodando uma aplicação **NodeJS** com interface web **HTML puro**, consumindo um banco **Mongo**, criando e listando objetos.


*Este projeto pode ser baixado através do link https://gitlab.com/ggodoisi/fiapdockerproject*


Para subir o projeto execute o comando na raiz do projeto:

> docker-compose up

Após a execução de todos os passos do composer uma mensagem dizendo que a aplicação conectou no banco de dados.

A aplicação irá subir na porta 80 da sua máquina. Acesse no navegador o link abaixo:

[http://localhost/](http://localhost/)

___


Os itens inseridos são salvos, expostos em um volume direcionado em uma pasta raiz do projeto chamada **data/**.

Então você poderá matar a execução do composer e subir novamente que os dados permanecerão acessíveis.

___


O banco de dados pode ser acessado através do host **localhost**, na porta **27017**. Para isto é necessário instalar um cliente do MongoDB.


Com o cliente padrão do MongoDb instalado execute o comando abaixo para conectar no banco de dados:

> mongo localhost/db


Ao acessar o banco de dados, digite o comando abaixo para listar as coleções:

> show collections


Será apresentado a coleção chamada **items**.

Para listar os dados digite o comando:

> db.items.find()

